/** @type {import('next').NextConfig} */
const nextConfig = {
  swcMinify: true,
  env: {
    SERVER_URL: process.env.SERVER_URL
  }
}

module.exports = nextConfig
