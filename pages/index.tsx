import type {GetServerSideProps, NextPage} from 'next'
import Home from "../app/components/home/Home";
import {IBtcPriceResponse} from "../app/types/bpi-response.type";
import {CurrencyService} from "../app/service/Currency.service";
import {useEffect} from "react";
import {useActions} from "../app/hooks/useActions";

interface IHomePage {
    btcPrice: IBtcPriceResponse | null
}

const HomePage: NextPage<IHomePage> = ({btcPrice}) => {

    const {setBtcInfo} = useActions()

    useEffect(() => {
        setBtcInfo(btcPrice)
    }, [])

    return (
        <Home/>
    )
}

export default HomePage

export const getServerSideProps: GetServerSideProps = async () => {
    try {
        const btc = await CurrencyService.forBTC()
        return {
            props: {
                btcPrice: btc
            }
        }
    } catch (e) {
        return {
            props: {
                btcPrice: null
            }
        }
    }
}