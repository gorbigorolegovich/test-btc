import {HistoryRateType} from "../types/historyRate.type";

export const historyRate = (newRate:number, currentRate:number, type:HistoryRateType):number => {
    switch (true) {
        case (newRate > currentRate):
            return type === 'max' ? newRate : currentRate
        case (newRate < currentRate):
            return type === 'min' ? newRate : currentRate
        default:
            return currentRate
    }
}