import styles from '../components/home/currency/HomeCurrancy.module.scss'

type ReturnType = {[style:string]: boolean} | undefined

export const choosePercentStatus = (rate:number):ReturnType => {
    switch (true){
        case (rate > 0):
            return {[styles.up]: true}
        case (rate < 0):
            return {[styles.down]: true}
        default:
            return undefined
    }
}