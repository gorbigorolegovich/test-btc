import {IBpi as IStateBpi} from "../store/btc/btcSlice.interface";
import {CodeType, IBpi as IActionBpi} from "../types/bpi-response.type";
import {historyRate} from "./historyRate";
import {calcPercentRate} from "./calcPercentRate";

export const mapBpiToState = (stateBpi: IStateBpi | null, actionBpi: IActionBpi): IStateBpi => {

    let newBpi = {} as IStateBpi
    const keys = Object.keys(actionBpi) as CodeType[]

    if (stateBpi) {
        keys.forEach((key) => {
            newBpi[key] = {
                ...actionBpi[key],
                maxRate: historyRate(+actionBpi[key].rate_float.toFixed(2), stateBpi[key].maxRate, 'max'),
                minRate: historyRate(+actionBpi[key].rate_float.toFixed(2), stateBpi[key].minRate, 'min'),
                standardRate: stateBpi[key].standardRate,
                percentRate: calcPercentRate(stateBpi[key].standardRate, actionBpi[key].rate_float)
            }
        })
    } else {

        keys.forEach((key) => {
            newBpi[key] = {
                ...actionBpi[key],
                maxRate: +actionBpi[key].rate_float.toFixed(2),
                minRate: +actionBpi[key].rate_float.toFixed(2),
                standardRate: actionBpi[key].rate_float,
                percentRate: 0
            }
        })
    }

    return newBpi
}