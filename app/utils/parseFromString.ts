export const parseFromString = (string:string):string | null => {

    return  new DOMParser().parseFromString(string,"text/html").documentElement.textContent

}