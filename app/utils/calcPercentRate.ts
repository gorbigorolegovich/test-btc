export const calcPercentRate = (statePercent: number, actionPercent: number):number => {

    return +(((actionPercent - statePercent) / statePercent) * 100)
        .toFixed(3)

}