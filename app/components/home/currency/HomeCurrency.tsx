import {FC, useMemo} from 'react';
import {ICurrencyWithHistory} from "../../../store/btc/btcSlice.interface";
import styles from './HomeCurrancy.module.scss'
import {choosePercentStatus} from "../../../utils/choosePercentStatus";
import CurrencyStatistics from "./statistics/CurrencyStatistics";
import CurrencyRateInfo from "./rate-info/CurrencyRateInfo";
import CurrencyCode from "./heading/CurrencyCode";

interface IHomeCurrency {
    currency: ICurrencyWithHistory
}

const HomeCurrency: FC<IHomeCurrency> = ({currency}) => {

    const percentStatus = useMemo(() => {
        return choosePercentStatus(currency.percentRate)
    }, [currency.percentRate])

    return (
        <div className={styles.wrapper}>
           <CurrencyCode code={currency.code} description={currency.description}/>
            <div className={styles.main_info}>
                <CurrencyRateInfo rate={currency.rate} symbol={currency.symbol}/>
                <CurrencyStatistics percentStatus={percentStatus}
                                    percentRate={currency.percentRate}
                                    symbol={currency.symbol}
                                    maxRate={currency.maxRate}
                                    minRate={currency.minRate}
                />
            </div>

        </div>

    );
};

export default HomeCurrency;