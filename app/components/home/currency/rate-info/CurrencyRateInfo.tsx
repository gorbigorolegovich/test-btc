import {FC, memo} from 'react';
import styles from "../HomeCurrancy.module.scss";
import {parseFromString} from "../../../../utils/parseFromString";
import {SymbolType} from "../../../../types/bpi-response.type";

interface IRateInfo {
    symbol: SymbolType
    rate:string
}

const CurrencyRateInfo:FC<IRateInfo> = ({symbol, rate}) => {
 return (
     <div className={styles.rate_info}>
         <div className={styles.symbol}>
             {parseFromString(symbol)}
         </div>
         <div className={styles.rate}>
             {rate}
         </div>
     </div>
 );
};

export default memo(CurrencyRateInfo);