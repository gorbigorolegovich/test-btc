import {FC, memo} from 'react';
import styles from "../HomeCurrancy.module.scss";
import cn from "classnames";
import {parseFromString} from "../../../../utils/parseFromString";
import {SymbolType} from "../../../../types/bpi-response.type";

interface ICurrencyStatistics {
    percentStatus?: { [p: string]: boolean }
    percentRate: number
    symbol: SymbolType
    minRate: number
    maxRate: number
}

const CurrencyStatistics: FC<ICurrencyStatistics> =
    ({percentStatus, percentRate, symbol, minRate, maxRate}) => {
        return (
            <div className={styles.statistics}>
                <div className={styles.statistics__item}>
                    <div>Form start %</div>
                    <div className={cn(styles.item__rate, percentStatus)}>{percentRate}%</div>
                </div>
                <div className={styles.statistics__item}>
                    <div>Form start Low</div>
                    <div className={styles.item__rate}>{parseFromString(symbol)}{minRate}</div>
                </div>
                <div className={styles.statistics__item}>
                    <div>Form start High</div>
                    <div className={styles.item__rate}>{parseFromString(symbol)}{maxRate}</div>
                </div>
            </div>
        );
    };

export default memo(CurrencyStatistics);