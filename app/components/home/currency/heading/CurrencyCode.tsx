import {FC, memo} from 'react';
import styles from "../HomeCurrancy.module.scss";

interface ICurrencyCode {
    code:string
    description:string
}

const CurrencyCode:FC<ICurrencyCode> = ({description, code}) => {
 return (
     <div className={styles.currency}>
         <div className={styles.code}>
             {code}
         </div>
         <div className={styles.description}>
             ({description})
         </div>
     </div>
 );
};

export default memo(CurrencyCode);