import {FC, useState} from 'react';
import {useInterval} from "../../hooks/useInterval";
import {CurrencyService} from "../../service/Currency.service";
import {useActions} from "../../hooks/useActions";
import {useSelector} from "react-redux";
import {CodeType} from "../../types/bpi-response.type";
import styles from './Home.module.scss'
import HomeControls from "./controls/HomeControls";
import HomeCurrency from "./currency/HomeCurrency";
import {selectBpi} from "../../store/selectors/selectBpi";

const Home: FC = () => {

    const bpi = useSelector(selectBpi)
    const {setBtcInfo} = useActions()
    const [currency, setCurrency] = useState<CodeType>('USD')
    const bpiKeys: CodeType[] = bpi ? Object.keys(bpi) as CodeType[] : []

    useInterval(async () => {
        const btc = await CurrencyService.forBTC()
        setBtcInfo(btc)
    }, 3000)

    return (
        <div className={styles.container}>
            <div className={styles.home}>
                <h1 className={styles.heading}>
                    <span className={styles.heading__name}>Bitcoin</span>
                    <span className={styles.heading__logo}>BTC</span>
                </h1>
                <HomeControls bpiKeys={bpiKeys} setCurrency={setCurrency}/>
                {bpi
                    ? <HomeCurrency currency={bpi[currency]}/>
                    : null
                }
            </div>
        </div>
    );
};

export default Home;