import {Dispatch, FC, SetStateAction} from 'react';
import styles from "./HomeCotrols.module.scss";
import Button from "../../ui/button/Button";
import {CodeType} from "../../../types/bpi-response.type";

interface IHomeControls {
    bpiKeys: CodeType[]
    setCurrency: Dispatch<SetStateAction<CodeType>>
}

const HomeControls:FC<IHomeControls> = ({bpiKeys, setCurrency}) => {
 return (
     <div className={styles.controls}>
         {
             bpiKeys.map((token) => {
                 return <Button key={token} onClick={() => {
                     setCurrency(token)
                 }}>{token}</Button>
             })
         }
     </div>
 );
};

export default HomeControls;