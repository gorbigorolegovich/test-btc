import {ButtonHTMLAttributes, FC, PropsWithChildren} from 'react';
import styles from './Button.module.scss'

interface IButton extends ButtonHTMLAttributes<HTMLButtonElement> {
}

const Button: FC<PropsWithChildren<IButton>> = ({type, children, ...rest}) => {
    return (
            <button className={styles.button} type={type} {...rest}>
                {children}
            </button>
    );
};

export default Button;
