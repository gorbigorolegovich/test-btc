import {IBtcPriceResponse} from "../types/bpi-response.type";

export class CurrencyService {

    static forBTC = async ():Promise<IBtcPriceResponse> => {
        const resp = await fetch(`${process.env.SERVER_URL}/v1/bpi/currentprice.json`)
        return await resp.json()
    }

}