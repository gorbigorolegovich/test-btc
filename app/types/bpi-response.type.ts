export interface IBtcPriceResponse {
    bpi: IBpi
    chartName: string
    disclaimer: string
    time: ITime
}

export interface IBpi extends Record<CodeType, ICurrency>{}

export interface ICurrency {
    code: CodeType
    symbol: SymbolType
    rate: string
    description: string
    rate_float: number
}
export interface ITime {
    updated: string
    updatedISO: string
    updateduk: string
}

export type CodeType = 'EUR' | 'GBP' | 'USD'
export type SymbolType = '&pound;' | '&#36;' | '&euro;'
