import {bindActionCreators} from "redux";
import {rootActions} from "../store/rootActions";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../store";


export const useActions = () => {

    const dispatch = useDispatch<AppDispatch>()

    return bindActionCreators(rootActions, dispatch)
}