import {useEffect, useRef} from "react";

export const useInterval = (callback: () => void, delay: number | null) => {
    const savedCallback = useRef<null | Function>(null);

    useEffect(() => {
        savedCallback.current = callback;
    });

    useEffect(() => {
        function tick() {
            if (typeof savedCallback.current === 'function')
                savedCallback.current();
        }

        if (delay !== null) {
            let id = setInterval(tick, delay);
            return () => clearInterval(id);
        }

    }, [delay]);
}