import {RootState} from "../index";
import {IBpi} from "../btc/btcSlice.interface";

export const selectBpi = (state:RootState):IBpi | null => state.btc.bpi
