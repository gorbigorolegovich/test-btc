import {combineReducers} from "redux";
import btcSlice from "./btc/btcSlice";


export const rootReducer = combineReducers({
    btc:btcSlice
})