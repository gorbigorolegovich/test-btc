import {CodeType, ICurrency, ITime} from "../../types/bpi-response.type";

export interface BtcSliceInterface {
    bpi: IBpi | null
    chartName: string
    disclaimer: string
    time: ITime | null
    error:string
}

export interface IBpi extends Record<CodeType, ICurrencyWithHistory>{}

export interface ICurrencyWithHistory extends ICurrency{
    standardRate:number
    minRate:number
    maxRate:number
    percentRate:number
}