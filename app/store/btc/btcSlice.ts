import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IBtcPriceResponse} from "../../types/bpi-response.type";
import {BtcSliceInterface} from "./btcSlice.interface";
import {mapBpiToState} from "../../utils/mapBpiToState";

const initialState: BtcSliceInterface = {
    bpi: null,
    chartName: '',
    time: null,
    disclaimer: '',
    error:''
}


const btcSlice = createSlice({
    name: 'btc',
    initialState,
    reducers: {
        setBtcInfo: (state, {payload}: PayloadAction<IBtcPriceResponse | null>) => {
            if (payload) {
                if (payload?.bpi) {
                    state.bpi = mapBpiToState(state.bpi, payload.bpi)
                }
                if (payload?.time) state.time = payload.time
                state.disclaimer = payload.disclaimer
                state.chartName = payload.chartName
            } else {
                state.error = 'Something wrong!'
            }
        }
    }
})

export default btcSlice.reducer
export const btcActions = btcSlice.actions